# Use an official Python runtime as a parent image for build
FROM python:3.7.8-slim-buster AS builder

# Set the working directory to /build
WORKDIR /build

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y binutils libc6 && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get autoremove -y && \
    pip install --upgrade pip && \
    pip install pipenv && \
    pip install pyinstaller

COPY . .

# Install any needed python packages and create executeable
RUN pipenv install --deploy --system --ignore-pipfile && \
	pyinstaller --clean --onefile hello.py


# Use debian-buster to run the executable
FROM debian:buster-slim AS runner

# Set the working directory to /app
WORKDIR /app

COPY --from=builder /build/dist/ dist/.
COPY runner.sh .

CMD ["/bin/bash", "/app/runner.sh"]