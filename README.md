# Pyinstaller Playground

## Introduction
This repository contains an example docker application using pyinstaller and multi-stage build

## Commands
1. Build the docker image: `docker build -t linux_playground:latest .`
1. Run the script inside the image: `docker run --rm linux_playground:latest`
1. Log into the running container : `docker run --rm -it linux_playground:latest /bin/bash`
